#!/usr/bin/env python
"""
path_lib.point - Geometry-related utility classes for
                 path-modifying extensions

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


TODO:

    Integrate unit selecter for angles as argument:

        >>> units
        'degrees'
        >>> type(units)
        <type 'str'>
        >>> if hasattr(math, units):
        ...     cmd = getattr(math, units)
        ...
        >>> cmd(math.pi)
        180.0
        >>> type(cmd)
        <type 'builtin_function_or_method'>
        >>>

"""
# pylint: disable=too-many-public-methods

# standard library
import math
import random


class Point(object):
    """Point base class for storing points in cartesian coordinates.

    Attributes:
        _x (float): x coordinate
        _y (float): x coordinate

    """

    def __init__(self, x=0, y=0):
        """Init Point based on cartesian coordinates x, y.

        Args:
            x (Optional[float]): x coordinate
            y (Optional[float]): y coordiante

        """
        self._x = float(x)
        self._y = float(y)

    # Special methods

    def __repr__(self):
        return 'Point(x={0}, y={1})'.format(self.x, self.y)

    def __str__(self):
        return '{0}, {1}'.format(self.x, self.y)

    def __eq__(self, other):
        assert isinstance(other, Point)
        return other.x == self.x and other.y == self.y

    def __ne__(self, other):
        assert isinstance(other, Point)
        return other.x != self.x and other.y != self.y

    def __lt__(self, other):
        assert isinstance(other, Point)
        return self.r < other.r

    def __le__(self, other):
        assert isinstance(other, Point)
        return self.r <= other.r

    def __gt__(self, other):
        assert isinstance(other, Point)
        return self.r > other.r

    def __ge__(self, other):
        assert isinstance(other, Point)
        return self.r >= other.r

    def __pos__(self):
        return Point(self.x, self.y)

    def __neg__(self):
        return Point(-self.x, -self.y)

    def __abs__(self):
        return Point(abs(self.x), abs(self.y))

    def __invert__(self):
        return Point(self.y, self.x)

    def __add__(self, other):
        assert isinstance(other, Point)
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        assert isinstance(other, Point)
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, factor):
        return Point(self.x * factor, self.y * factor)

    def __div__(self, quotient):
        return Point(self.x / quotient, self.y / quotient)

    def __radd__(self, other):
        return self + other

    def __rsub__(self, other):
        return (-self) + other

    def __rmul__(self, other):
        return self * other

    def __rdiv__(self, other):
        return self / other

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y

    def __imul__(self, other):
        new = self * other
        self.x = new.x
        self.y = new.y

    def __idiv__(self, other):
        new = self / other
        self.x = new.x
        self.y = new.y

    def __bool__(self):
        return bool(self.x) or bool(self.y)

    # python 2 compat
    __nonzero__ = __bool__

    def __iter__(self):
        return self.__list__().__iter__()

    def __list__(self):
        return [self.x, self.y]

    def __dict__(self):
        return {"x": self.x, "y": self.y}

    def __copy__(self):
        return Point(self.x, self.y)

    def __deepcopy__(self, memo):
        return Point(self.x, self.y)

    # Properties with setters

    @property
    def x(self):
        """Get or set the cartesian coordinate x."""
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        """Get or set the cartesian coordinate y."""
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    @property
    def r(self):
        """Get or set the polar distance r (radius).

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.sqrt(self.x ** 2 + self.y ** 2)

    @r.setter
    def r(self, radius):
        theta = self.t
        self.x = math.cos(theta) * radius
        self.y = math.sin(theta) * radius

    @property
    def t(self):
        """Get or set the polar angle t (theta) in radians.

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.atan2(self.y, self.x)

    @t.setter
    def t(self, theta):
        radius = self.r
        self.x = math.cos(theta) * radius
        self.y = math.sin(theta) * radius

    @property
    def t_deg(self):
        """Get or set the polar angle t (theta) in degrees.

        The polar coordinates r, t are converted from/to
        properties x, y.
        """
        return math.degrees(self.t)

    @t_deg.setter
    def t_deg(self, theta):
        self.t = math.radians(theta)

    # Methods changing instance attribute values

    def normalize(self):
        """Normalize self to length 1."""
        self.r = 1

    def translate(self, delta_x=0, delta_y=0):
        """Move current Point instance by dx, dy."""
        self.x += delta_x
        self.y += delta_y

    def move(self, x=None, y=None):
        """Move current Point instance to x, y."""
        self.x = x if x is not None else self.x
        self.y = y if y is not None else self.y

    def extend(self, delta_r=0):
        """Extend current Point along radius by delta_r.

        The polar angle t is unchanged.
        """
        self.r += delta_r

    def rotate(self, delta_t=0, other=None):
        """Rotate current Point by delta_t in radians.

        The rotation center is the origin (0,0) or an optional other
        Point instance.
        """
        if other is None:
            self.t += delta_t
        else:
            assert isinstance(other, Point)
            new = other + Polar(other.distance(self),
                                other.angle(self) + delta_t)
            self.r = new.r
            self.t = new.t

    def rotate_deg(self, delta_t=0, other=None):
        """Rotate current Point by delta_t in degrees.

        The rotation center is the origin (0,0) or an optional other
        Point instance.
        """
        if other is None:
            self.t_deg += delta_t
        else:
            assert isinstance(other, Point)
            new = other + Polar(other.distance(self),
                                other.angle(self) + math.radians(delta_t))
            self.r = new.r
            self.t = new.t

    def flip_h(self):
        """Flip current Point instance across y-axis."""
        self.x = -self.x

    def flip_v(self):
        """Flip current Point instance across x-axis."""
        self.y = -self.y

    # Methods returning computed or formatted values

    @property
    def cartesian(self):
        """Get the cartesian coordinates of Point instance as list.

        (readonly property)
        """
        return [self.x, self.y]

    @property
    def polar(self):
        """Get the polar coordinates of Point instance as list.

        The polar angle t is returned in radians.

        (readonly property)
        """
        return [self.r, self.t]

    @property
    def polar_deg(self):
        """Get the polar coordinates of Point instance as list.

        The polar angle t is returned in degrees.

        (readonly property)
        """
        return [self.r, self.t_deg]

    def perpendicular(self):
        """Return perpendicular vector to self."""
        return Point(-self.y, self.x)

    def distance(self, other):
        """Return the distance between self and other."""
        assert isinstance(other, Point)
        return (other - self).r

    def distances(self, *args):
        """Return list of distances from self to multiple others."""
        return [self.distance(other) for other in args]

    def angle(self, other):
        """Return the angle of vector connecting self and other."""
        assert isinstance(other, Point)
        return (other - self).t

    def angles(self, *args):
        """Return list of angles between self and multiple others."""
        return [self.angle(other) for other in args]

    def sweep(self, other1, other2):
        """Return sweep angle between self and two others."""
        assert isinstance(other1, Point)
        assert isinstance(other2, Point)
        return (other1 - self).t - (other2 - self).t

    def format_move(self):
        """Return string (SVG path commands) to move to self."""
        return 'M {} '.format(self)

    def format_line(self):
        """Return string (SVG path commands) to draw a line to self."""
        return 'L {} '.format(self)

    def format_arc(self, radius, large_arc, sweep):
        """Return string (SVG path commands) to draw an arc to self."""
        return 'A {0},{0} 0 {1} {2} {3} '.format(radius,
                                                 large_arc,
                                                 sweep,
                                                 self)

    @staticmethod
    def format_close():
        """Return string (SVG path commands) to close (sub-)path."""
        return 'Z '


class Polar(Point):
    """Point sub-class for points in polar coordinates (radius, theta)."""

    def __init__(self, radius=0.0, theta=0.0):
        """Init Point based on polar coordinates r, t.

        Args:
            r (Optional[float]): polar coordinate r (radius)
            t (Optional[float]): polar coordinate t (theta) in radians

        Note:
            The polar coordinates are converted to cartesian
            coordinates and stored as Point() attributes _x, _y.
        """
        Point.__init__(self)
        self._x = math.cos(theta) * radius
        self._y = math.sin(theta) * radius


def csp_to_points(csp, sub=0, nodes=None):
    """Convert csp into list of Point instances without control points."""
    if nodes is None:
        nodes = len(csp[sub])
    return [[Point(csp[1][0], csp[1][1]) for csp in subs]
            for subs in csp][sub][:nodes]


def randomize(point, rx=10.0, ry=10.0, norm=True):
    """Return point with randomized cartiesian coordinates x, y.

    Args:
        rx[float]: radius x
        ry[float]: radius y
        norm[bool]: use normalized distribution
    """
    # pylint: disable=invalid-name
    if norm:
        frx = abs(random.normalvariate(0.0, 0.5*rx))
        fry = abs(random.normalvariate(0.0, 0.5*ry))
    else:
        frx = random.uniform(0.0, rx)
        fry = random.uniform(0.0, ry)
    a = random.uniform(0.0, 2*math.pi)
    if 0:  # FIXME: (using-constant-test)
        point.x += math.cos(a)*frx
        point.y += math.sin(a)*fry
    else:
        return Point(point.x + math.cos(a)*frx, point.y + math.sin(a)*fry)


def randomize2(point, rx=10.0, ry=10.0, dist="Uniform"):
    """Return point with randomized cartesian coordinates x, y.

    Args:
        rx[float]: radius x
        ry[float]: radius y
        dist[string]: type of random distribution
                      values: Gaussian | Pareto | Lognorm | Unifom
    """
    # pylint: disable=invalid-name
    if dist == "Gaussian":
        r1 = random.gauss(0.0, rx)
        r2 = random.gauss(0.0, ry)
    elif dist == "Pareto":
        # Sign is used ot fake a double sided pareto distribution.
        # For parameter value between 1 and 2 the distribution has
        # infinite variance.  I truncate the distribution to a high
        # value and then normalize it.  The idea is to get spiky
        # distributions, any distribution with long-tails is good
        # (ideal would be Levy distribution).
        sign = random.uniform(-1.0, 1.0)
        r1 = min(random.paretovariate(1.0), 20.0) / 20.0
        r2 = min(random.paretovariate(1.0), 20.0) / 20.0
        r1 = rx * math.copysign(r1, sign)
        r2 = ry * math.copysign(r2, sign)
    elif dist == "Lognorm":
        sign = random.uniform(-1.0, 1.0)
        r1 = rx * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)
        r2 = ry * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)
    elif dist == "Uniform":
        r1 = random.uniform(-rx, rx)
        r2 = random.uniform(-ry, ry)
    if 0:  # FIXME: (using-constant-test)
        point.x += r1
        point.y += r2
    else:
        return Point(point.x + r1, point.y + r2)


def perpendicular(point):
    """Return perpendicular vector of point."""
    return point.perpendicular()


def randomize_midpoint(point1, point2, smoothness=4.0):
    """Return randomized midpoint based on smoothness parameter."""
    # pylint: disable=invalid-name
    diff = (point2 - point1)
    length = diff.r
    perp = perpendicular(diff).normalize()
    f = random.uniform(-length / (1+smoothness), length / (1+smoothness))
    perp *= f
    diff.r = length / 2.0
    if 0:  # FIXME: (using-constant-test)
        new = diff + perp
        point1.x = new.x
        point1.y = new.y
    else:
        return diff + perp


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
