#!/usr/bin/env python
"""
path_lib.common - Common utility functions and base classes for
                  path-modifying extensions

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=global-statement
# pylint: disable=unused-import
# pylint: disable=wrong-import-order
# pylint: disable=wrong-import-position

# standard library
import re

# compat
import six

# local library
import inkex
import cubicsuperpath
import path_lib.transform as mat


try:
    inkex.localize()
except AttributeError:
    import gettext
    _ = gettext.gettext


# Global "constants"
SVG_SHAPES = ('rect', 'circle', 'ellipse', 'line', 'polyline', 'polygon')
HAVE_NUMPY = False
# FIXME: Globals which may change
DEBUG = False


try:
    import numpy
    HAVE_NUMPY = True
except ImportError as error_msg:
    pass


# Utiliy functions for selection checking

def zSort(inNode, idList):
    """Z-sort selected objects, return list of IDs."""
    # pylint: disable=invalid-name
    # TODO: include function in shared module simpletransform.py
    sortedList = []
    theid = inNode.get("id")
    if theid in idList:
        sortedList.append(theid)
    for child in inNode:
        if len(sortedList) == len(idList):
            break
        sortedList += zSort(child, idList)
    return sortedList


def is_group(node):
    """Check node for group tag."""
    return node.tag == inkex.addNS('g', 'svg')


def is_path(node):
    """Check node for path tag."""
    return node.tag == inkex.addNS('path', 'svg')


def is_basic_shape(node):
    """Check node for SVG basic shape tag."""
    return node.tag in (inkex.addNS(tag, 'svg') for tag in SVG_SHAPES)


def is_custom_shape(node):
    """Check node for Inkscape custom shape type."""
    return inkex.addNS('type', 'sodipodi') in node.attrib


def is_shape(node):
    """Check node for SVG basic shape tag or Inkscape custom shape type."""
    return is_basic_shape(node) or is_custom_shape(node)


def has_path_effect(node):
    """Check node for Inkscape path-effect attribute."""
    return inkex.addNS('path-effect', 'inkscape') in node.attrib


def is_modifiable_path(node):
    """Check node for editable path data."""
    return is_path(node) and not (has_path_effect(node) or
                                  is_custom_shape(node))


def is_image(node):
    """Check node for image tag."""
    return node.tag == inkex.addNS('image', 'svg')


# Utiliy functions for image loading and saving

def create_img_node():
    """Create a new <image> node, without any data yet."""
    node = inkex.etree.Element(inkex.addNS('image', 'svg'))
    node.set('x', "0")
    node.set('y', "0")
    node.set('preserveAspectRatio', "none")
    node.set('image-rendering', "optimizeSpeed")
    return node


def create_img_placeholder(img_node):
    """Insert new path based on geometry of <image> node."""
    image_d = 'm {0},{1} h {2} v {3} h -{2} z'.format(img_node.get('x'),
                                                      img_node.get('y'),
                                                      img_node.get('width'),
                                                      img_node.get('height'))
    path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    path.set('d', image_d)
    path.set('style', "fill:none;stroke:none")
    path.set('transform', img_node.get('transform', ""))
    index = img_node.getparent().index(img_node)
    img_node.getparent().insert(index, path)
    return path


def get_image_scale(image, img_node):
    """Return image scale (image pixel size : <image> node size)."""
    scale_x = scale_y = 1.0
    if image is not None and is_image(img_node):
        scale_x = image.size[0] / float(img_node.get('width', image.size[0]))
        scale_y = image.size[1] / float(img_node.get('height', image.size[1]))
    return (scale_x, scale_y)


def check_req():
    """Check general requirements for path-modifying classes."""
    pass


# Utiliy functions for wrapping the result

def wrap_group(node):
    """Wrap node in group, return group."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    index = node.getparent().index(node)
    node.getparent().insert(index, group)
    group.append(node)
    return group


def group_with(node, path):
    """Put path into group wrapped around node, return group."""
    group = wrap_group(node)
    if group is not None:
        mat.apply_absolute_diff(node, path)
        group.append(path)
    return group


# Utility functions for transforms

def mat_path_to_img_node(path, img_node, fit=False):
    """Return mat which transforms from path to img_node coords."""
    # 1 apply path transform
    # 2 apply path parent transform
    # 3 reverse img_node parent transform
    mat1 = mat.copy_from(path) if not fit else mat.IDENT_MAT
    mat2 = mat.absolute(path.getparent())
    mat3 = mat.invert(mat.absolute(img_node.getparent()))
    return mat.compose_triplemat(mat1, mat2, mat3)


def mat_img_node_to_image(img_node, image, fit=False):
    """Return mat which transforms from img_node to image coords."""
    # 1 reverse img_node transform
    # 2 if not fit: reverse img_node offset
    # 3 scale
    mat1 = mat.invert(mat.copy_from(img_node))
    mat2 = mat.invert(mat.offset(img_node)) if not fit else mat.IDENT_MAT
    mat3 = mat.scale(*get_image_scale(image, img_node))
    return mat.compose_triplemat(mat1, mat2, mat3)


def mat_image_to_img_node(image, img_node, fit=False):
    """Return mat which transforms from image to img_node coords."""
    # 1 scale
    # 2 if not fit: apply img_node offset
    # 3 apply img_node transform
    mat1 = mat.invert(mat.scale(*get_image_scale(image, img_node)))
    mat2 = mat.offset(img_node) if not fit else mat.IDENT_MAT
    mat3 = mat.copy_from(img_node)
    return mat.compose_triplemat(mat1, mat2, mat3)


def mat_img_node_to_path(img_node, path, fit=False):
    """Return mat which transforms from img_node to path coords."""
    # 1 apply img_node parent transform
    # 2 reverse path parent transform
    # 3 ??? reverse path transform
    mat1 = mat.absolute(img_node.getparent())
    mat2 = mat.invert(mat.absolute(path.getparent()))
    # TODO: verify mat3 for fit option
    mat3 = mat.invert(mat.copy_from(path)) if not fit else mat.IDENT_MAT
    return mat.compose_triplemat(mat1, mat2, mat3)


# Compensate / apply preserved and parent transforms of helper paths and images
# Note: parameter 'fit' currently only used by image_perspective.py

def transform_path_to_image(path, img_node, image, fit=False):
    """Return mat which transforms from path to image coords."""
    # 1 path to img_node
    # 2 img_node to image
    mat1 = mat_path_to_img_node(path, img_node, fit)
    mat2 = mat_img_node_to_image(img_node, image, fit)
    return mat.compose_doublemat(mat2, mat1)


def transform_image_to_path(image, img_node, path, fit=False):
    """Return mat which transforms from image to path coords."""
    # 1 image to img_node
    # 2 img_node to path
    mat1 = mat_image_to_img_node(image, img_node, fit)
    mat2 = mat_img_node_to_path(img_node, path, fit)
    return mat.compose_doublemat(mat2, mat1)


# specific helper functions

def combine_two_paths(path0_path1):
    """Combine two paths into a new one.

    Combine the transformed csp representations as sub-paths, insert
    new path element with path data from csp path into document and
    return the etree element.
    """
    path0, path1 = path0_path1
    csp = []
    for path in (path0, path1):
        csp_path = cubicsuperpath.parsePath(path.get('d'))
        mat.apply_to(mat.copy_from(path), csp_path)
        csp.append(csp_path)
    # transform path1 into path0 coords
    mat.apply_to(mat.absolute_diff(path0, path1), csp[1])
    # compensate preserved transform of path0 in csp1
    mat.apply_to(mat.invert(mat.copy_from(path0)), csp[1])
    # combine the two csp paths (append csp1 as sub-path to csp0)
    csp[0].append(csp[1][0])
    # insert as new path into document
    path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    path.set('d', cubicsuperpath.formatPath(csp[0]))
    # insert before source path
    index = path0.getparent().index(path0)
    path0.getparent().insert(index, path)
    # return new path
    return path


def csp_to_points(dest, sub, points):
    """Convert csp path into a list of nodes without control points."""
    if points is None:
        points = len(dest[sub])
    return [[(csp[1][0], csp[1][1]) for csp in subs]
            for subs in dest][sub][:points]


def find_perspective_coeffs(pb, pa):
    """Calculate and return perspective coefficients.

    Based on formula from <http://stackoverflow.com/a/14178717>.
    """
    # pylint: disable=invalid-name
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])
    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)
    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)


# Effect classes

class PathEffecter(inkex.Effect):
    """Effect class for projecting a path based on a quadrilateral."""

    def __init__(self):
        """Init inkex.Effect class, parse default options."""
        inkex.Effect.__init__(self)

        # support legacy documents (allow dpi override)
        self.OptionParser.add_option("--override_dpi",
                                     action="store",
                                     type="string",
                                     dest="override_dpi",
                                     default="pass",
                                     help="Override internal dpi")
        # global settings
        self.OptionParser.add_option("--verbose",
                                     action="store",
                                     type="inkbool",
                                     dest="verbose",
                                     default=True,
                                     help="Verbose mode")
        # tabs
        self.OptionParser.add_option("--tab",
                                     action="store",
                                     type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    # -----------------------------------------------------------------
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    #

    # Maintain separate dicts for 90 and 96 dpi to allow extensions to
    # implement support for legacy (or future) documents.
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.543307086614174,
        'cm': 35.43307086614174,
        'm':  3543.307086614174,
        'km': 3543307.086614174,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0
    }

    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.333333333333333,
        'px': 1.0,
        'mm': 3.779527559055119,
        'cm': 37.79527559055119,
        'm':  3779.527559055119,
        'km': 3779527.559055119,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0
    }

    # A dictionary of unit to user unit conversion factors.
    __uuconv = __uuconv_96dpi

    # New method: get_document_scale()
    # ================================
    # The new method supports arbitrary uniform scale factors and is
    # used in the unit conversion methods unittouu() and uutounit().

    def match_uuconv(self, val, eps=0.01):
        """Fuzzy matching of value to one of the known unit factors."""
        match = None
        for key in self.__uuconv:
            if inkex.are_near_relative(self.__uuconv[key], val, eps):
                match = self.__uuconv[key]
        return match or val

    def split_svg_length(self, string, percent=False):
        """Split SVG length string into float and unit string."""
        # pylint: disable=invalid-name
        param = unit = None
        if percent:
            unit_list = '|'.join(list(self.__uuconv.keys()) + ['%'])
        else:
            unit_list = '|'.join(list(self.__uuconv.keys()))
        param_re = re.compile(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_re = re.compile(
            '({0})$'.format(unit_list))
        if string is not None:
            p = param_re.match(string)
            u = unit_re.search(string)
            if p:
                try:
                    param = float(p.string[p.start():p.end()])
                except (KeyError, ValueError):
                    pass
            if u:
                try:
                    unit = u.string[u.start():u.end()]
                except KeyError:
                    pass
        return (param, unit)

    def get_page_dimension(self, attribute):
        """Retrieve value for SVGRoot attribute passed as argument.

        Return list of float and string.
        """
        string = self.document.getroot().get(attribute, "100%")
        val, unit = self.split_svg_length(string, percent=True)
        dimension = 100.0 if val is None else val
        dimension_unit = 'px' if unit is None else unit
        return (dimension, dimension_unit)

    def get_viewbox(self):
        """Retrieve value for viewBox from current document.

        Return list of 4 floats or None.
        """
        viewbox_attribute = self.document.getroot().get('viewBox', None)
        viewbox = []
        if viewbox_attribute:
            try:
                viewbox = list(float(i) for i in viewbox_attribute.split())
            except ValueError:
                pass
        if len(viewbox) != 4 or (viewbox[2] < 0 or viewbox[3] < 0):
            viewbox = None
        return viewbox

    def check_viewbox(self, width, w_unit, height, h_unit):
        """Verify values for SVGRoot viewBox width and height.

        Return list of 2 floats.
        """
        viewbox = self.get_viewbox()
        if viewbox is None:
            # If viewBox attribute is missing or invalid: calculate
            # viewBox dimensions corresponding to width, height
            # attributes. Treat '%' as special case, else assume 96dpi.
            vb_width = (width if w_unit == '%' else
                        self.__uuconv[w_unit] * width)
            vb_height = (height if h_unit == '%' else
                         self.__uuconv[h_unit] * height)
        else:
            vb_width, vb_height = viewbox[2:4]
        return (vb_width, vb_height)

    def get_aspectratio(self):
        """Get preserveAspectRatio from SVGRoot.

        Return list of 2 strings.
        """
        preserve_aspect_ratio = self.document.getroot().get(
            'preserveAspectRatio', "xMidYMid meet")
        try:
            return preserve_aspect_ratio.split()
        except ValueError:
            # Nothing to split: Set <meetOrSlice> parameter to default
            # 'meet'.  Note that the <meetOrSlice> parameter is ignored
            # if <align> parameter is 'none'.
            return (preserve_aspect_ratio, "meet")

    def get_aspectratio_scale(self, width, w_unit, height, h_unit,
                              vb_width, vb_height):
        """Calculate offset, scale based on SVGRoot preserveAspectRatio.

        Return list of 4 floats.
        """
        # pylint: disable=too-many-arguments
        # pylint: disable=too-many-locals

        x_offset = y_offset = 0.0
        scale = 1.0
        aspect_align, aspect_clip = self.get_aspectratio()
        if aspect_align == 'none':
            # TODO: implement support for non-uniform scaling
            # based on preserveAspectRatio attribute.
            pass
        else:
            width = vb_width * (width / 100.0) if w_unit == '%' else width
            height = vb_height * (height / 100.0) if h_unit == '%' else height
            vb_width = width if vb_width == 0 else vb_width
            vb_height = height if vb_width == 0 else vb_height
            scale_x = width / vb_width
            scale_y = height / vb_height
            # Force uniform scaling based on <meetOrSlice> parameter
            scale = (aspect_clip == "meet" and
                     min(scale_x, scale_y) or
                     max(scale_x, scale_y))
            # calculate offset based on <align> parameter
            align_x = aspect_align[1:4]
            align_y = aspect_align[5:8]
            offset_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
            try:
                # TODO: verify units of calculated offsets
                x_offset = round(
                    offset_factor[align_x] * (width - vb_width*scale), 3)
                y_offset = round(
                    offset_factor[align_y] * (height - vb_height*scale), 3)
            except KeyError:
                pass
        return (x_offset, y_offset, scale, scale)

    def apply_viewbox(self):
        """Return offset (x, y) and scale for width, height of SVGRoot."""

        width, w_unit = self.get_page_dimension('width')
        height, h_unit = self.get_page_dimension('height')
        vb_width, vb_height = self.check_viewbox(
            width, w_unit, height, h_unit)
        x_offset, y_offset, scale_x, scale_y = self.get_aspectratio_scale(
            width, w_unit, height, h_unit, vb_width, vb_height)

        # Treat '%' as special case, else apply scale to conversion
        # factor from __uuconv.  Use match_uuconv() to allow precision
        # tolerance of the document's page dimensions.
        w_scale, h_scale = [(scale if unit == '%' else
                             self.match_uuconv(self.__uuconv[unit] * scale))
                            for unit, scale in (
                                (w_unit, scale_x), (h_unit, scale_y))]

        return (x_offset, y_offset, w_scale, h_scale)

    def get_document_scale(self):
        """Return document scale factor.

        Calculate scale based on these SVGRoot attributes:
        'width', 'height', 'viewBox', 'preserveAspectRatio'
        """
        unitfactor = self.__uuconv['px']  # fallback
        width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
        return width_unitfactor or height_unitfactor or unitfactor

    # Unit conversion tools
    # =====================
    # Methods to assist unit handling in extensions / derived classes.

    def unittouu(self, string):
        """Return userunits given a string representation in units.

        Return float or None.
        """
        val = None
        if string is not None:
            val, unit = self.split_svg_length(string)
            if val is None:
                val = 0.0
            if unit is None:
                val /= self.get_document_scale()  # Assume default 'px'.
            elif unit in self.__uuconv.keys():
                val *= (self.__uuconv[unit] / self.get_document_scale())
        return val

    def uutounit(self, val, unit):
        """Return value in userunits converted to other units."""
        return val / (self.__uuconv[unit] / self.get_document_scale())

    def switch_uuconv(self, dpi):
        """Allow extensions to override internal resolution."""
        if dpi == '90':
            self.__uuconv = self.__uuconv_90dpi
        elif dpi == '96':
            self.__uuconv = self.__uuconv_96dpi
        else:  # unchanged
            pass

    #
    #
    # Copied from lp:~inkscape.dev/inkscape/extensions-svgunitfactor
    #
    # -----------------------------------------------------------------

    def get_inkscape_version(self):
        """Return Inkscape version extracted from version attribute."""
        # NOTE: r14965 changed how this attribute is updated:
        # Inkscape no longer updates the attribute on load but on save.
        # New files thus don't have such an attribute anymore.
        # quick workaround: default to (assumed) 0.92
        inkscape_version_string = self.document.getroot().get(
            inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(
            r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    def create_layer(self, name):
        """Create custom layer with ID name, return etree element."""
        new_layer = inkex.etree.Element(inkex.addNS('g', 'svg'))
        new_layer.set('id', self.uniqueId(name))
        new_layer.set(inkex.addNS('groupmode', 'inkscape'), "layer")
        new_layer.set(inkex.addNS('label', 'inkscape'), name)
        self.document.getroot().append(new_layer)
        return new_layer

    def showme(self, msg):
        """Verbose output."""
        if self.options.verbose:
            inkex.debug(msg)

    def effect(self):
        """Initiate extension-side actions for current class instance."""
        raise NotImplementedError


class PathProjecter(PathEffecter):
    """Effect class for projecting a path based on a quadrilateral."""

    def __init__(self):
        """Init base class."""
        PathEffecter.__init__(self)

    def project_path(self, source=None, path=None, dest=None):
        """Core method defined in each path-modifying extension."""
        raise NotImplementedError

    def effect(self):
        """Check selection for path projection (source, dest)."""
        # pylint: disable=line-too-long
        if len(self.options.ids) < 2:
            inkex.errormsg(_("This extension requires two selected paths."))
        else:
            # get z-sorted list of ids
            id_list = zSort(self.document.getroot(), self.options.ids)
            # obj is bottom-most
            obj = self.selected[id_list[0]]
            # helper is top-most
            helper = self.selected[id_list[-1]]

            if is_custom_shape(obj):
                inkex.errormsg(_(
                    'The bottom-most object is of type {0}.\n'.format(
                        obj.get(inkex.addNS('type', 'sodipodi'))) +
                    'Try using the procedure Path->Object to Path.'))

            elif is_modifiable_path(obj) or is_group(obj):
                if is_path(helper):
                    helper_csp = cubicsuperpath.parsePath(helper.get('d'))
                    if len(helper_csp) < 1 or len(helper_csp[0]) < 4:
                        inkex.errormsg(_(
                            "This extension requires that the top-most path " +
                            "be four nodes long."))
                    else:
                        # everything's ok, run extension's effect
                        self.project_path(obj, helper, helper_csp)
                else:
                    if is_group(helper):
                        inkex.errormsg(_(
                            "The top-most object is a group, not a path.\n" +
                            "Try using the procedure Object->Ungroup."))
                    else:
                        inkex.errormsg(_(
                            "The top-most object is not a path.\n" +
                            "Try using the procedure Path->Object to Path."))
            else:
                inkex.errormsg(_(
                    "The bottom-most object is not a path.\n" +
                    "Try using the procedure Path->Object to Path."))


class PathModifier(PathEffecter):
    """Effect class for path-modifying extensions."""

    def __init__(self):
        """Init base class, parse optins for PathModifier."""
        PathEffecter.__init__(self)

        self.valid = []
        self.skipped = []
        self.groups = []

        self.target_layer = self.__class__.__name__

        self.documentscale = None
        self.viewboxwidth = None
        self.viewboxheight = None

        # selection scope
        self.OptionParser.add_option("--recursive",
                                     action="store",
                                     type="inkbool",
                                     dest="recursive",
                                     default=False,
                                     help="Recurse into groups")

    def set_dpi(self, dpi):
        """Support 90 and 96 dpi."""
        if dpi == 'default':
            ink_ver = self.get_inkscape_version()
            # TODO: compare numerically to >= 0.92 for future releases
            if ink_ver == '0.91+devel' or ink_ver == '0.92':
                self.switch_uuconv('96')
            else:  # Assume older version with 90dpi
                self.switch_uuconv('90')
        else:
            self.switch_uuconv(dpi)
        # reset information about scale saved by overloaded methods
        self.documentscale = None
        self.viewboxwidth = None
        self.viewboxheight = None

    def get_class_layer(self):
        """Return class layer with ID stored in self.target_layer."""
        class_layer = self.getElementById(self.target_layer)
        if class_layer is None or not is_group(class_layer):
            class_layer = self.create_layer(self.target_layer)
            self.target_layer = class_layer.get('id')
        return class_layer

    def report(self, mode="skipped"):
        """Report count of skipped (or valid) objects.

        Warn about empty selection.
        """
        # pylint: disable=line-too-long
        total = len(self.valid) + len(self.skipped)
        if len(self.skipped):
            if mode == "skipped":
                inkex.errormsg(_(
                    '{0} out of {1} objects '.format(
                        len(self.skipped), total) +
                    'in the selection have been skipped because ' +
                    'they are not paths or have a path effect applied.'))
            if mode == "valid":
                if len(self.valid):
                    inkex.errormsg(_(
                        '{0} out of {1} objects '.format(
                            len(self.valid), total) +
                        'in the selection have been modified.'))
                else:
                    inkex.errormsg(_(
                        'No modifiable paths found among {0} objects '.format(
                            total) +
                        'in the selection.'))
            if len(self.groups):
                inkex.errormsg(_(
                    '\nTotal count of groups processed: {0}'.format(
                        len(self.groups))))
        elif not total:
            inkex.errormsg(_(
                'This extension requires a selection of one or more paths.'))

    def modify_path(self, node):
        """Core method defined in each path-modifying extension."""
        raise NotImplementedError

    def type_check(self, node):
        """Check type specific to sub-class PathModifier."""
        # pylint: disable=no-self-use
        return is_modifiable_path(node)

    def recurse_selection(self, id_, node):
        """Recursively process selected objects.

        Actions:
            check object type
            track actions in lists
        """
        if is_group(node) and self.options.recursive:
            self.groups.append(id_)
            for child in node:
                self.recurse_selection(child.get('id'), child)
        elif self.type_check(node):
            self.valid.append(id_)
        else:
            self.skipped.append(id_)

    def effect(self):
        """Run initial loop for selected objects, report (optional)."""
        self.set_dpi(self.options.override_dpi)
        for id_, node in six.iteritems(self.selected):
            self.recurse_selection(id_, node)
        for id_ in self.valid:
            # run extension's effect for every valid object
            self.modify_path(self.getElementById(id_))
        if self.options.verbose:
            self.report()


class PathGenerator(PathModifier):
    """Effect class for generating paths based on path data."""

    def __init__(self):
        """Init base class."""
        PathModifier.__init__(self)

    def report(self, mode="skipped"):
        """Report count of skipped (or valid) objects.

        Warn about empty selection.
        """
        # pylint: disable=line-too-long
        total = len(self.valid) + len(self.skipped)
        if len(self.skipped):
            if mode == "skipped":
                inkex.errormsg(_(
                    '{0} out of {1} objects '.format(
                        len(self.skipped), total) +
                    'in the selection have been skipped because ' +
                    'they have no path data to serve as generator.'))
            if mode == "valid":
                if len(self.valid):
                    inkex.errormsg(_(
                        '{0} out of {1} objects '.format(
                            len(self.valid), total) +
                        'in the selection have been used as generators.'))
                else:
                    inkex.errormsg(_(
                        'No paths found among {0} objects '.format(total) +
                        'in the selection.'))
            if len(self.groups):
                inkex.errormsg(_(
                    '\nTotal count of groups processed: {0}'.format(
                        len(self.groups))))
        elif not total:
            inkex.errormsg(_(
                'This extension requires a selection of one or more paths.'))

    def generate_path(self, node):
        """Core method defined in each path-generating extension."""
        raise NotImplementedError

    def modify_path(self, node):
        self.generate_path(node)

    def type_check(self, node):
        """Check type specific to sub-class PathGenerator."""
        # pylint: disable=no-self-use
        return is_path(node)


class PathConvertor(PathModifier):
    """Effect class for path-converting extensions."""

    def __init__(self):
        """Init base class."""
        PathModifier.__init__(self)

        # convertor options
        self.OptionParser.add_option("--mode",
                                     action="store",
                                     type="string",
                                     dest="mode",
                                     default="insert_new",
                                     help="Mode")

    # overload method from base class to store scale as instance attribute
    def get_document_scale(self):
        """Return document scale."""
        if self.documentscale is None:
            unitfactor = 1.0  # fallback 'px'
            width_unitfactor, height_unitfactor = self.apply_viewbox()[2:4]
            self.documentscale = (width_unitfactor or
                                  height_unitfactor or
                                  unitfactor)
        return self.documentscale

    def get_viewbox_height(self):
        """Return page height in SVG user units."""
        if self.viewboxheight is None:
            width, w_unit = self.get_page_dimension('width')
            height, h_unit = self.get_page_dimension('height')
            self.viewboxwidth, self.viewboxheight = self.check_viewbox(
                width, w_unit, height, h_unit)
        return self.viewboxheight

    def convert_path(self, node):
        """Core method defined in each path-converting extension."""
        raise NotImplementedError

    def modify_path(self, node):
        return self.convert_path(node)

    def type_check(self, node):
        """Check type specific to sub-class PathConvertor."""
        if self.options.mode == "replace_d":
            return is_modifiable_path(node)
        else:
            return is_path(node)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
