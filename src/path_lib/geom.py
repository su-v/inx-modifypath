#!/usr/bin/env python
"""
path_lib.geom - Geometry-related utility functions for
                path-modifying extensions

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name

# standard library
import random
import math

# local library
import bezmisc


# ----- misc

def nodelist_to_csp(alist):
    """Convert list of nodes into csp path."""
    csp = []
    if alist:
        csp.append([])  # create first sub-path
        for node in alist:
            csp[-1].append([])  # append empty node list
            for _ in range(3):
                csp[-1][-1].append([float(node[0]), float(node[1])])
    return csp


def csp_to_nodelist(csp, sub=0, nodes=None):
    """Convert csp into a list of nodes without control points."""
    if nodes is None:
        nodes = len(csp[sub])
    return [[(csp[1][0], csp[1][1]) for csp in subs]
            for subs in csp][sub][:nodes]


# ----- segment lengths

def count_segments(csp):
    """Return number of segments in csp path (sum over all sub-paths."""
    return sum([len(p)-1 for p in csp])


def csp_segment_length(sp1, sp2, tolerance=0.001):
    """Return length of bezier segment (measured along path)."""
    bez = (sp1[1][:], sp1[2][:], sp2[0][:], sp2[1][:])
    return bezmisc.bezierlength(bez, tolerance)


def csp_lengths(csp):
    """Return tuple with segment lengths list, total length."""
    total = 0
    lengths = []
    for subpath in csp:
        lengths.append([])
        for i in xrange(1, len(subpath)):
            l = csp_segment_length(subpath[i-1], subpath[i])
            lengths[-1].append(l)
            total += l
    return lengths, total


def count_csp_lengths(csplen):
    """Return count of ssegment lengths measured."""
    retval = 0
    for subpath in csplen:
        for l in subpath:
            if l > 0:
                retval += 1
    return retval


def report_average_segment_lengths(csp):
    """Report average segment length of csp path."""
    lengths, total = csp_lengths(csp)
    avg_length = total / count_csp_lengths(lengths)
    # sys.stderr.write('average segment length: {0}\n'.format(avg_length))
    return avg_length


# ----- interpolate points

def point_at_percent(x1_y1, x2_y2, percent):
    """TODO."""
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    percent /= 100.0
    return point_at_t((x1, y1), (x2, y2), percent)


def point_at_t(x1_y1, x2_y2, t=0.5):
    """TODO."""
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    return [x1 + t*(x2 - x1), y1 + t*(y2 - y1)]


# ----- split segments

def csp_bezier_split(sp1, sp2, t=0.5):
    """Return csp split at offset t (along path)."""
    m1 = point_at_t(sp1[1], sp1[2], t)
    m2 = point_at_t(sp1[2], sp2[0], t)
    m3 = point_at_t(sp2[0], sp2[1], t)
    m4 = point_at_t(m1, m2, t)
    m5 = point_at_t(m2, m3, t)
    m = point_at_t(m4, m5, t)
    return [[sp1[0][:], sp1[1][:], m1],
            [m4, m, m5],
            [m3, sp2[1][:], sp2[2][:]]]


def csp_bezier_split_at_length(sp1, sp2, l=0.5, tolerance=0.001):
    """Return csp path fragment with inserted node, offset l along path."""
    bez = (sp1[1][:], sp1[2][:], sp2[0][:], sp2[1][:])
    t = bezmisc.beziertatlength(bez, l, tolerance)
    return csp_bezier_split(sp1, sp2, t)


# ----- Randomize points or segment midpoints

def randomize_point(x_y, rx, ry, norm):
    """Return randomized point coordinates."""
    x, y = x_y
    if norm:
        rrx = abs(random.normalvariate(0.0, 0.5*rx))
        rry = abs(random.normalvariate(0.0, 0.5*ry))
    else:
        rrx = random.uniform(0.0, rx)
        rry = random.uniform(0.0, ry)
    a = random.uniform(0.0, 2*math.pi)
    x += math.cos(a)*rrx
    y += math.sin(a)*rry
    return [x, y]


def randomize_point_2(x_y, rx, ry, dist="Uniform"):
    """Return variation of point coordinates x, y."""
    x, y = x_y
    if dist == "Gaussian":
        r1 = random.gauss(0.0, rx)
        r2 = random.gauss(0.0, ry)
    elif dist == "Pareto":
        # Sign is used ot fake a double sided pareto distribution.
        # For parameter value between 1 and 2 the distribution has
        # infinite variance.  I truncate the distribution to a high
        # value and then normalize it.  The idea is to get spiky
        # distributions, any distribution with long-tails is good
        # (ideal would be Levy distribution).
        sign = random.uniform(-1.0, 1.0)

        r1 = min(random.paretovariate(1.0), 20.0) / 20.0
        r2 = min(random.paretovariate(1.0), 20.0) / 20.0

        r1 = rx * math.copysign(r1, sign)
        r2 = ry * math.copysign(r2, sign)
    elif dist == "Lognorm":
        sign = random.uniform(-1.0, 1.0)
        r1 = rx * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)
        r2 = ry * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)
    elif dist == "Uniform":
        r1 = random.uniform(-rx, rx)
        r2 = random.uniform(-ry, ry)
    x += r1
    y += r2
    return [x, y]


def randomize_segment(x1, y1, x2, y2, smoothness):
    """Return randomized midpoint based on smoothness parameter."""
    # Calculate the vector from (x1,y1) to (x2,y2)
    x3 = x2 - x1
    y3 = y2 - y1
    # Calculate the point half-way between the two points
    hx = x1 + x3/2
    hy = y1 + y3/2
    # Calculate normalized vector perpendicular to the vector (x3,y3)
    length = math.sqrt(x3*x3 + y3*y3)
    if length != 0:
        nx = -y3/length
        ny = x3/length
    else:
        nx = 1
        ny = 0
    # Scale perpendicular vector by random factor
    r = random.uniform(-length/(1+smoothness), length/(1+smoothness))
    nx = nx * r
    ny = ny * r
    # add scaled perpendicular vector to the half-way point to get the final
    # displaced subdivision point
    x = hx + nx
    y = hy + ny
    return [x, y]


# ----- end misc

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
