#!/usr/bin/env python
"""
path2polyline - convert SVG path to SVG polyline

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
import inkex
import path_lib.common as modpath
import path_lib.geom as pg
import path_lib.point as pp
import path_lib.transform as mat
import cubicsuperpath
import simpletransform


def formatted(f, precision=8):
    """Pretty-printer for floats, return formatted string."""
    # pylint: disable=invalid-name
    fstring = '.{0}f'.format(precision)
    return format(f, fstring).rstrip('0').rstrip('.')


def format_nodelist(alist, pr=8):
    """Format list of nodes as <polyline> 'points' value."""
    # pylint: disable=invalid-name
    return ' '.join([','.join([formatted(c, pr) for c in p]) for p in alist])


def is_closed(_):
    """NYI."""
    return False


def csp2cusp(csp):
    """Convert nodes to cusp nodes in csp path."""
    return pg.nodelist_to_csp(pg.csp_to_nodelist(csp))


def csp2poly(csp):
    """Create polyline based on csp path, return etree element"""
    if is_closed(csp[0]):
        path2poly = inkex.etree.Element(inkex.addNS('polygon', 'svg'))
    else:
        path2poly = inkex.etree.Element(inkex.addNS('polyline', 'svg'))
    path2poly.set('points', format_nodelist(pg.csp_to_nodelist(csp)))
    return path2poly


def debug_csp(csp, debug=False):
    """TODO."""
    # temporarily setting options
    debug = False  # self.options.debug
    if debug:
        points = pp.csp_to_points(csp)
        inkex.debug(points)
        nodelist = pg.csp_to_nodelist(csp)
        inkex.debug(nodelist)


class PathToPolyline(modpath.PathConvertor):
    """PathConvertor sub-class to convert <path> to <polyline>."""

    def __init__(self):
        """Init base class."""
        modpath.PathConvertor.__init__(self)
        # class attributes
        self.target_layer = self.__class__.__name__
        # path2poly options
        self.OptionParser.add_option("--copy_style",
                                     action="store",
                                     type="inkbool",
                                     dest="copy_style",
                                     default=False,
                                     help="Copy style from path to node")
        self.OptionParser.add_option("--export_points",
                                     action="store",
                                     type="inkbool",
                                     dest="export_points",
                                     default=False,
                                     help="Copy style from path to node")
        self.OptionParser.add_option("--relative_to",
                                     action="store",
                                     type="string",
                                     dest="relative_to",
                                     default="root",
                                     help="Coordinates relative to")
        self.OptionParser.add_option("--coord_system",
                                     action="store",
                                     type="string",
                                     dest="coord_system",
                                     default="svg",
                                     help="Coordinate system")
        self.OptionParser.add_option("--units",
                                     action="store",
                                     type="string",
                                     dest="units",
                                     default="uu",
                                     help="Units")
        self.OptionParser.add_option("--precision",
                                     action="store",
                                     type="int",
                                     dest="precision",
                                     default=3,
                                     help="Precision")

    def export_coordinates(self, node, csp):
        """Export node coordinates via stderr."""
        # Viewport
        if self.options.relative_to != 'node':
            # Apply preserved transforms of node.
            mat.apply_copy_from(node, csp)
        if self.options.relative_to == 'root':
            # Apply composed parent transforms of node's parent
            mat.apply_to(mat.absolute(node.getparent()), csp)

        # Coordinate system (useful with 'root'):
        if self.options.coord_system == 'desktop':
            # move vertically by negative viewBox height
            mat1 = simpletransform.parseTransform(
                'translate(0,-{0})'.format(self.get_viewbox_height()))
            # flip vertically
            mat2 = simpletransform.parseTransform('scale(1,-1)')
            mat.apply_to(mat.compose_doublemat(mat2, mat1), csp)

        # Document scale, output units:
        if self.options.units == 'css_px':
            scale = self.get_document_scale()
            mat1 = simpletransform.parseTransform('scale({0})'.format(scale))
            mat.apply_to(mat1, csp)
        elif self.options.units == 'display':
            doc_unit = self.getNamedView().get(
                inkex.addNS('document-units', 'inkscape'))
            if doc_unit is not None:
                scale = self.unittouu('1{0}'.format(doc_unit))
                mat1 = simpletransform.parseTransform(
                    'scale({0})'.format(1/scale))
                mat.apply_to(mat1, csp)
        elif self.options.units != 'uu':
            scale = self.unittouu('1{0}'.format(self.options.units))
            mat1 = simpletransform.parseTransform('scale({0})'.format(1/scale))
            mat.apply_to(mat1, csp)

        # send point coordinates to stderr
        inkex.debug(
            format_nodelist(pg.csp_to_nodelist(csp), self.options.precision))

    def replace_d(self, node, csp):
        """Replace path data with cusp nodes from csp data."""
        # Convert nodes in csp path to cusp nodes with retracted handles.
        new_csp = []
        for subpath in csp:
            new_csp.append(csp2cusp([subpath])[0])
        node.set('d', cubicsuperpath.formatPath(new_csp))
        # Return point list via stderr.
        if self.options.export_points:
            inkex.debug(format_nodelist(pg.csp_to_nodelist(new_csp)))

    def create_polyline(self, node, csp):
        """Create polyline element with path nodes as points."""
        # Compensate preserved transforms on node.
        mat.apply_copy_from(node, csp)
        # Get target layer to append/insert to.
        if self.options.mode == "insert_new":
            # Compensate node parent's transforms.
            mat.apply_to(mat.absolute(node.getparent()), csp)
            target_layer = self.get_class_layer()
        else:  # self.options.mode == "insert"; fallback for unknown
            target_layer = node.getparent()
        # Create polyline for each sub-path.
        for subpath in csp:
            new_poly = csp2poly([subpath])
            if self.options.mode == "replace_element":
                index = node.getparent().index(node)
                node.getparent().insert(index, new_poly)
            else:
                target_layer.append(new_poly)
            # Copy style from path to polyline.
            if self.options.copy_style:
                new_poly.set('style', node.get('style'))
            # Return point list via stderr.
            if self.options.export_points:
                inkex.debug(new_poly.get('points'))
        # Clean-up if replacing path element with polyline(s).
        if self.options.mode == "replace_element":
            node.getparent().remove(node)

    def convert_to_poly(self, node, csp):
        """Convert path nodes into cusp nodes or polyline points."""
        if self.options.mode == "replace_d":
            self.replace_d(node, csp)
        else:
            self.create_polyline(node, csp)

    def convert_path(self, node):
        """Parse path, convert to cusp nodes used as points for polyline."""

        # Convert path data to cubicsuperpath.
        csp = cubicsuperpath.parsePath(node.get('d'))

        if self.options.tab == '"options"':
            self.convert_to_poly(node, csp)
        elif self.options.tab == '"export"':
            self.export_coordinates(node, csp)
        else:  # No action for help or unknown tab.
            pass


if __name__ == '__main__':
    ME = PathToPolyline()
    ME.affect()


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
